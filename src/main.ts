import { createApp } from 'vue'
import { setupRouter } from './router'
import { setupStore } from './store'
import App from './App.vue'

// 启动函数
const bootstrap = () => {
    const app = createApp(App)
    // 安装初始化路由
    setupRouter(app)
    // 安装初始化store
    setupStore(app)
    // 挂载
    app.mount('#app')
}

// 启动
bootstrap()
