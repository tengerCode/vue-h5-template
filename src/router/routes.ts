import type { RouteRecordRaw } from 'vue-router'

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        redirect: '/home',
        component: () => import('@/App.vue'),
        children: [
            {
                path: '/home',
                component: () => import('@/pages/home.vue'),
            },
        ],
    },
    {
        path: '/login',
        component: () => import('@/pages/Login/index.vue'),
    },
]

export default routes
