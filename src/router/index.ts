import { createRouter, createWebHistory } from 'vue-router'
import type { App } from 'vue'
import routes from './routes.ts'

const router = createRouter({
    history: createWebHistory('/'),
    routes: routes,
})

/*
 * Info:全局守卫,守卫路由的切换,路由在切换的时候会调用该方法
 *
 * */
// router.beforeEach((to, from, next) => {

// })

export const setupRouter = (app: App<Element>) => {
    app.use(router)
}

export default router
